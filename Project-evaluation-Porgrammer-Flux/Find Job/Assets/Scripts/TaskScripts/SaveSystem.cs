using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class DataToSave
{
    public int ShootingSelected;
    public bool WithShield;
    public int Damage;
    public int CurrentLevel;
    public int EnemiesDead;
    public int Points;
    public float Life;
}

namespace SaveSystem
{
    public static class SaveSystem<T>
    {
        static FileStream file = null;
        static BinaryFormatter bf = new BinaryFormatter();
        static string destinationName = "/SaveInformations";
        public static void SaveGame(T saveInfo)
        {
            file = File.Create(Application.persistentDataPath + destinationName);
            bf.Serialize(file, saveInfo);
            file.Close();
        }

        public static T LoadGameInfo()
        {
            file = File.Open(Application.persistentDataPath + destinationName, FileMode.Open);
            T data = (T)bf.Deserialize(file);
            file.Close();
            return data;
        }

        public static bool HasDataToLoad()
        {
            return File.Exists(Application.persistentDataPath + destinationName);
        }

        public static void RemoveAllDataSaved()
        {
            if (HasDataToLoad())
                File.Delete(Application.persistentDataPath + destinationName);
        }
    }
}