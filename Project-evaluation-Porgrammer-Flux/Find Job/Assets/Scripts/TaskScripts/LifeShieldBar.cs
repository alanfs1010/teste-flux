using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeShieldBar : MonoBehaviour
{
    public ControlShip playerController;
    public Image lifeBar;

    private void Update()
    {
        lifeBar.fillAmount = playerController.GetLifeShield() / 10;
    }
}
