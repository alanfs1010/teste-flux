using SaveSystem;
using UnityEngine;
using UnityEngine.UI;
using Gaminho;
using UnityEngine.SceneManagement;

public class ContinueButton : MonoBehaviour
{
    public Button continueButton;

    private void Start()
    {
        continueButton.interactable = SaveSystem<DataToSave>.HasDataToLoad();
    }

    public void ContinueClick()
    {
        Statics.LoadData();
        SceneManager.LoadScene("Game");
    }

}
