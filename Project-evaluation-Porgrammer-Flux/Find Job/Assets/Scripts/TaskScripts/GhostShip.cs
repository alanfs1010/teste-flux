using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GhostShip : MonoBehaviour
{
    public Transform playerShip;
    public float persuitDelay = 0.025f;
    private void FixedUpdate()
    {
        if (playerShip.position != transform.position || playerShip.rotation != transform.rotation)
        {
            transform.DOKill(); 
            transform.DOMove(playerShip.position, persuitDelay);
            transform.DORotate(playerShip.rotation.eulerAngles, persuitDelay); 
        }
    }
}
