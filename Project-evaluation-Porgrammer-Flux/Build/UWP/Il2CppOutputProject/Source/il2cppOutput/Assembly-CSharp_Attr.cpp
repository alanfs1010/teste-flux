﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ControlGame_t311D836B3FE34B0182B188639EC992A3B193DC96_CustomAttributesCacheGenerator_TextStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_0_0_0_var), NULL);
	}
}
static void EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_0_0_0_var), NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var), NULL);
	}
}
static void Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var), NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var), NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var), NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var), NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_0_0_0_var), NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_0_0_0_var), NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11__ctor_m39F55ED326225F6F70DE169AD2263FA6E50709F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_IDisposable_Dispose_m2AE1C5C8A05729FFB6C5CC4DFE94459847EE2B64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C8E17E4297E6D9C06BF4208D35C9F28F798858F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_IEnumerator_Reset_mD9B6FA38233812B8AB5C3217C1DD75370731DEC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_IEnumerator_get_Current_m2FA6F373AFD979C18AF6AE19551B73A40AF765C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13__ctor_m509660EF6044E6F0298FBA22453E53E7EBB151EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_IDisposable_Dispose_m3E97D1DF4970C68E17C32FBA15BCEBC114B5E28B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE590286AC0CAFC35ABE9B65A43290FCB2AE0BC1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_IEnumerator_Reset_m72662B20826DEE0604D743F4AEF9BE8B21998231(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_IEnumerator_get_Current_mDC23A241C783E9DD2018A924D0994E0E720CF33D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DestroyTime_t24806801B4F6EA764796D3C1789E9D157D0023D2_CustomAttributesCacheGenerator_DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var), NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Explosion_t03ACA9CD2F46E4913DC45A3EA50B165F4662DCED_CustomAttributesCacheGenerator_Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var), NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ControlShip_tDE8B063AEEACBC4110A6A51AF8533BED76E0D3C8_CustomAttributesCacheGenerator_ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_0_0_0_var), NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15__ctor_m2B8AEC7F4C367018734F07D04F33AF1D457AECB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_IDisposable_Dispose_m5A84EFE10951A4AB77CC01EB739D428507BC9D61(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70974FE0B089361CAD462BCE365A3FB4E9337C4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_m10AA0124EE17DB5A2A71839446D7DD301A7FC818(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_m9263C839D099A97D92EA9E46C93CDBCD50FE96B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t8EDD49424F7AFC055DC1442B3F99B3BFCF6B09F0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tA5DAB4AB298719A5FC9203633FE7CAE2D7AFBF0A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tB2A5900BBCFDCC67A4CCA370F4284F7494E958E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tF06F41913CFED04E5FC3E2C676BCEAC2D3E53CAC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t5BD4116FD80F4DAD54FD39BBD7A2CFBDAEAB3AF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t35A2F555A5E408DEE2EA63254AACB729C014757D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t39BE4FE1476B4D5D73FC150406C576C638973F8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t3A8BE7E3EE8D0963B5FC3AE4F184CAF2EB8E1D84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tC3DF99D3D2F6A0C0E6651294AE0DF0537CEC4617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t97B2374B522E403B5DF1B457D5B6EBF73C0D5CC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tF8E56709396C6DFFECA7608E4CD67CBE2C1C1CE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t7D774AA07804F8A87A6F27483038CEC5FB0CC440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tED64664CBF4D43290FDDCBF237DF29F9DB8F92A9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tDABE9F243A4095C0C2295D15C58B0D3334267A3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tB7DA75EB7F11962B20C503C1B2A50BF4D821E318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tB63C203014F824EA9466F064AF2D8A157A7F4CBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tC3FB3913A0B5BA22F06134B01808D6D94A5F618E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t440FEF8A7D3454EC9E176430ADEF6185E58594EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t9AB75C89DF5C6CBF983D29D70DA8497E2A734640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_t1DAB80471E2CE447FD9DC4EFC18B5D4E48D8120A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_0_0_0_var), NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator_U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator_U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator_U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator_U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator_U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator_U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[268] = 
{
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t8EDD49424F7AFC055DC1442B3F99B3BFCF6B09F0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tA5DAB4AB298719A5FC9203633FE7CAE2D7AFBF0A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tB2A5900BBCFDCC67A4CCA370F4284F7494E958E1_CustomAttributesCacheGenerator,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tF06F41913CFED04E5FC3E2C676BCEAC2D3E53CAC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t5BD4116FD80F4DAD54FD39BBD7A2CFBDAEAB3AF3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t35A2F555A5E408DEE2EA63254AACB729C014757D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t39BE4FE1476B4D5D73FC150406C576C638973F8D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t3A8BE7E3EE8D0963B5FC3AE4F184CAF2EB8E1D84_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tC3DF99D3D2F6A0C0E6651294AE0DF0537CEC4617_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t97B2374B522E403B5DF1B457D5B6EBF73C0D5CC9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tF8E56709396C6DFFECA7608E4CD67CBE2C1C1CE5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t7D774AA07804F8A87A6F27483038CEC5FB0CC440_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tB9266040A99BBE64FC737A175355345A13E86D21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t6B6DFEAD8FF1DD6A724C294AA0B120F82A7A6937_CustomAttributesCacheGenerator,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tED64664CBF4D43290FDDCBF237DF29F9DB8F92A9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tDABE9F243A4095C0C2295D15C58B0D3334267A3A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tB7DA75EB7F11962B20C503C1B2A50BF4D821E318_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tB63C203014F824EA9466F064AF2D8A157A7F4CBF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tC3FB3913A0B5BA22F06134B01808D6D94A5F618E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t440FEF8A7D3454EC9E176430ADEF6185E58594EF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t9AB75C89DF5C6CBF983D29D70DA8497E2A734640_CustomAttributesCacheGenerator,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_t1DAB80471E2CE447FD9DC4EFC18B5D4E48D8120A_CustomAttributesCacheGenerator,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator,
	U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator,
	U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator,
	U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator,
	U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator,
	U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator,
	ControlGame_t311D836B3FE34B0182B188639EC992A3B193DC96_CustomAttributesCacheGenerator_TextStart,
	EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_t71016A10790CE995349F35F697F784B6C726E051_CustomAttributesCacheGenerator_U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_t0BF59ED4A6D214279882298BAE1927F9D4059B35_CustomAttributesCacheGenerator_U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11__ctor_m39F55ED326225F6F70DE169AD2263FA6E50709F0,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_IDisposable_Dispose_m2AE1C5C8A05729FFB6C5CC4DFE94459847EE2B64,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C8E17E4297E6D9C06BF4208D35C9F28F798858F,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_IEnumerator_Reset_mD9B6FA38233812B8AB5C3217C1DD75370731DEC6,
	U3CKillMeU3Ed__11_t75C53EAB5AD6A430C5F5D7A90352F892AD92CE26_CustomAttributesCacheGenerator_U3CKillMeU3Ed__11_System_Collections_IEnumerator_get_Current_m2FA6F373AFD979C18AF6AE19551B73A40AF765C6,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13__ctor_m509660EF6044E6F0298FBA22453E53E7EBB151EF,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_IDisposable_Dispose_m3E97D1DF4970C68E17C32FBA15BCEBC114B5E28B,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE590286AC0CAFC35ABE9B65A43290FCB2AE0BC1D,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_IEnumerator_Reset_m72662B20826DEE0604D743F4AEF9BE8B21998231,
	U3CShootU3Ed__13_t79772A0ECA4A827BC7D0047073B75792F5DDF45C_CustomAttributesCacheGenerator_U3CShootU3Ed__13_System_Collections_IEnumerator_get_Current_mDC23A241C783E9DD2018A924D0994E0E720CF33D,
	DestroyTime_t24806801B4F6EA764796D3C1789E9D157D0023D2_CustomAttributesCacheGenerator_DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_t03ACA9CD2F46E4913DC45A3EA50B165F4662DCED_CustomAttributesCacheGenerator_Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	ControlShip_tDE8B063AEEACBC4110A6A51AF8533BED76E0D3C8_CustomAttributesCacheGenerator_ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15__ctor_m2B8AEC7F4C367018734F07D04F33AF1D457AECB4,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_IDisposable_Dispose_m5A84EFE10951A4AB77CC01EB739D428507BC9D61,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70974FE0B089361CAD462BCE365A3FB4E9337C4B,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_m10AA0124EE17DB5A2A71839446D7DD301A7FC818,
	U3CShootU3Ed__15_tA3D5CDD11652E4A5CA95A8EFBFC8113B6662B07E_CustomAttributesCacheGenerator_U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_m9263C839D099A97D92EA9E46C93CDBCD50FE96B5,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1,
	DOTweenModuleAudio_tDC7DB487C2129D2F83D97EB958A6A39BDD55B1D3_CustomAttributesCacheGenerator_DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7,
	DOTweenModulePhysics_tFCC295D4FA2826D2BA7BB5B6BB1A9B28ECCF4036_CustomAttributesCacheGenerator_DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092,
	DOTweenModulePhysics2D_t7C53FEA66B3404F5031A765A783A70301B01B6B2_CustomAttributesCacheGenerator_DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25,
	U3CAsyncWaitForCompletionU3Ed__10_tAF83373BEFDCE5CDEE1D8D2213CE8CD7384CAC24_CustomAttributesCacheGenerator_U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819,
	U3CAsyncWaitForRewindU3Ed__11_t3CC63CE2CBCA49F9CADC6697FDDE8026B073132A_CustomAttributesCacheGenerator_U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64,
	U3CAsyncWaitForKillU3Ed__12_t69F4C18D4B977504979616766BA83349830366AB_CustomAttributesCacheGenerator_U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_t7E11D4778BC9859A55D5F850CD6DE389D4AC372A_CustomAttributesCacheGenerator_U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47,
	U3CAsyncWaitForPositionU3Ed__14_t8C34DB89D763058E32F5FECE7E62363A58303AE9_CustomAttributesCacheGenerator_U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E,
	U3CAsyncWaitForStartU3Ed__15_t6B840A7C6ECC2E009C3BC88B5DC9C937FBD80571_CustomAttributesCacheGenerator_U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
